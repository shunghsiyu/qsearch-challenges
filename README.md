# Qsearch Challenges

## Questions

1. 利用Python (2.7 or 3.5+皆可) 取得 ptt 線上使用者列表中unique的代號1000個
(練習查資料並依任務導向修改code的能力) hint: telnetlib

    * 完成繳交方式：錄執行狀況，並於實體面試詢問

2. 用Python (2.7 or 3.5+皆可) 抓取 Dcard 美妝版每小時抓取一次新文章，並將蒐集到的新文章每小時紀錄一次互動數，持續24小時，因此每個網站至少會有24個資料點，資料可用CSV方式儲存。 (使用requests及bs4持續訪問特定網站並安排snapshot流程的能力)

    * 完成繳交方式：實體面試時檢視 CSV 檔案。

3. 從2的結果中找出24小時內累績最多互動數的5篇文章，並將互動數依紀錄時間畫出折線圖？(整理資料並呈現結果的能力)

4. 給予一台 CPU 僅有，CPU 600MHz ，memory 僅有128MB的Google Compute Engine Linux 機器(有連外網)，有一個包含 3 億列數值的檔案，資料放在Linux 掛載的硬碟上，如何有機會在 5分鐘內將該數值檔案內的數值有小到大做好排序？

    * 完成繳交方式：在此說明

5. 若今日 Youbike 廠商提供201804月的騎乘資料給我們，我們怎麼驗證對方提供的資料可能有漏？

    * 完成繳交方式：在此說明

# References

## Challenge 1

* [\[使用手冊\] 查詢網友、登入次數顏色、註冊顯示 @6484](https://www.ptt.cc/bbs/PttNewhand/M.1416925895.A.136.html)
* [\[建議\] 一個好像存在很久的bug](https://www.ptt.cc/bbs/PCman/M.1234157729.A.275.html)
* [\[問題\] 批踢踢Crawler](https://www.ptt.cc/bbs/Python/M.1337415833.A.33C.html)
* [\[問題\] Telnetlib做BBS Crawler?](https://www.ptt.cc/bbs/Python/M.1364380345.A.27E.html)
* [BSS Crawler for Taiwan](https://www.slideshare.net/Buganini/bbs-crawler-for-taiwan)
* [Python 學習筆記 : telnetlib 模組測試](http://yhhuang1966.blogspot.com/2018/11/python-telnetlib.html)
* [PTTCrawler](https://github.com/g21589/PTTCrawler)
* [PttAutoLoginPost](https://github.com/twtrubiks/PttAutoLoginPost)
* [PTTLibrary](https://github.com/Truth0906/PTTLibrary)
* [PTT.cc 如何使用 UTF-8 的模式登入](https://blog.longwin.com.tw/2017/03/ptt-cc-telnet-utf8-login-2017/)
* [fabric - runners.py](https://github.com/fabric/fabric/blob/2.0/fabric/runners.py#L47-L59)
* [Control codes - converting to ASCII, Hex or Decimal](https://www.windmill.co.uk/ascii-control-codes.html)
* [keydown() in nally/YLView.mm](https://github.com/yllan/nally/blob/master/Code/YLView.mm#L645)
* [TerminalEscapeCodes](https://geoffg.net/Downloads/Terminal/TerminalEscapeCodes.pdf)
* [取得 big5-uao 編碼的文章/信件內容](https://github.com/Truth0906/PTTLibrary/issues/11)
* [pttbbs/common/sys/vtkbd.c](https://github.com/ptt/pttbbs/blob/master/common/sys/vtkbd.c)
* [pttbbs/include/vtkbd.h](https://github.com/ptt/pttbbs/blob/7e580b6434f22e57c194c9f59482c29582ef62b4/include/vtkbd.h)
* [ANSI\_escape\_code](https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_codes)
* [ASCII Table and Description](http://www.asciitable.com/)
* [Converting Control Codes to ASCII, Decimal and Hexadecimal](https://www.windmill.co.uk/ascii-control-codes.html)
* [Map CMD Up/Down/Left/Right to PgUp/PgDn/Home/End](https://github.com/yllan/nally/commit/8dca776c95569d3972903cec70683e11d937fd1a)


## Challenge 2

* [Beautiful Soup Documentation](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [Requests: HTTP for Humans™](https://2.python-requests.org/en/master/)
* [Is there a CSS selector by class prefix?](https://stackoverflow.com/a/8588532)
