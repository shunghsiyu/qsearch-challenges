#!/usr/bin/env python3
from collections import namedtuple
import csv
from datetime import datetime, timedelta
import fcntl
import os
from bs4 import BeautifulSoup
import dramatiq
from dramatiq.brokers.redis import RedisBroker
import requests


BASE_URL = 'https://www.dcard.tw'
LATEST_MAKEUP_URL = f'{BASE_URL}/f/makeup?latest=true'
FIELDS = ['url', 'iteration', 'title', 'reaction_count', 'retrieval_date']
INTERVAL = {'hours': 1}
DELAY_SECONDS = timedelta(**INTERVAL).total_seconds()
OUTPUT_CSV = os.environ.get('OUTPUT_CSV', 'output.csv')
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')

redis_broker = RedisBroker(url=f'redis://{REDIS_HOST}:6379/0')
dramatiq.set_broker(redis_broker)

class ReactionEntry(namedtuple('ReactionEntry', FIELDS)):
    @classmethod
    def from_row(cls, row):
        url = row[0]
        iteration = int(row[1])
        title = row[2]
        reaction_count = int(row[3])
        retrieval_date = row[4]
        return cls(url, count, title, reaction_count, retrieval_date)

def posted_datetime(post):
    """Return the datetime that this post was posted, in UTC timezone."""
    date_posted = post.select_one('span[class^="PostEntry_published"], span[class*=" PostEntry_published"]')
    d = datetime.strptime(date_posted.text, '%m月%d日 %H:%M')
    return d.replace(year=datetime.utcnow().year)

def new_posts(posts, since):
    for post in posts:
        posted_at = posted_datetime(post)
        delta = posted_at - since
        # It is possible that we ran after the scheduled time and it should be
        # fine, we'll pickup the ignored article on the next run.
        if timedelta() <= delta < timedelta(**INTERVAL):
            yield post

def parse_post(post):
    url = post['href']
    title = post.select_one('[class^="PostEntry_title_"], [class*=" PostEntry_title_"]').text
    reaction = post.select_one('[class^="PostEntry__LikeCount"], [class*=" PostEntry__LikeCount"]')
    return url, title, int(reaction.text)

def get_latest(last_retrieval):
    res = requests.get(LATEST_MAKEUP_URL)
    retrieval = res.headers['Date']
    soup = BeautifulSoup(res.content, 'html.parser')
    posts = soup.select('a[class^="PostEntry_root_"], a[class*=" PostEntry_root_"]')
    for post in new_posts(posts, last_retrieval):
        url, title, reaction_count = parse_post(post)
        yield ReactionEntry(url, 1, title, reaction_count, retrieval)

@dramatiq.actor
def process_latest(last_retrieval_str):
    last_retrieval = datetime.strptime(last_retrieval_str, "%Y-%m-%dT%H:%M:%S")
    current_retrieval = last_retrieval + timedelta(**INTERVAL)
    process_latest.send_with_options(
        args=(current_retrieval.isoformat(timespec='seconds'),),
        delay=DELAY_SECONDS * 1000)

    for entry in get_latest(last_retrieval):
        print('new:', entry)
        write_entry(OUTPUT_CSV, entry)
        process_update.send_with_options(
            args=(entry,),
            delay=DELAY_SECONDS * 1000)

@dramatiq.actor(max_retries=3)
def process_update(fields):
    old_entry = ReactionEntry(*fields)
    entry = update_entry(old_entry)
    print('update:', entry)
    if entry.iteration < 24:
        process_update.send_with_options(
            args=(entry,),
            delay=DELAY_SECONDS * 1000)

    write_entry(OUTPUT_CSV, entry)
    
def update_entry(entry):
    res = requests.get(BASE_URL + entry.url)
    assert res.status_code == 200
    retrieval = res.headers['Date']
    soup = BeautifulSoup(res.content, 'html.parser')
    reaction = soup.select_one('div[class^="PostFooter__LikeCount"], div[class*=" PostFooter__LikeCount"]')
    reaction_count = int(reaction.text)
    return entry._replace(iteration=entry.iteration + 1, reaction_count=reaction_count, retrieval_date=retrieval)

def write_entry(filename, entry):
    with open(filename, mode='a', newline='') as f:
        # Ensure only one process is writing to the file
        fcntl.flock(f, fcntl.LOCK_EX)
        try:
            writer = csv.writer(f)
            writer.writerow(entry)
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
    
def main():
    last_interval = datetime.utcnow() - timedelta(**INTERVAL)
    process_latest(last_interval.isoformat(timespec='seconds'))


if __name__ == '__main__':
    main()
