#!/usr/bin/env python3
from contextlib import contextmanager
import curses.ascii as asci
from os import environ
import time
import re
import warnings
import paramiko
import pyte


BUFF_SIZE = 2 ** 16
CONTINIOUS_POLL = 0.3

# Default terminal size on most OS
WIDTH = 80
HEIGHT = 24

REFRESH = asci.ctrl('l')
ENTER = '\r'

# From https://geoffg.net/Downloads/Terminal/TerminalEscapeCodes.pdf
ESC = chr(asci.ESC)
ESC_SEQ = ESC + '['
LEFT = ESC_SEQ + 'D'
PAGEDOWN = ESC_SEQ + '6~'

ACCOUNT = {
    'id': environ['PTT_ID'],
    'passwd': environ['PTT_PASSWD'],
}

USERNAME_REGEX = re.compile(r'●?\s+\d+(?:\s+.)?\s+(\w+)')

class PTT:
    """A naive PTT client with limited functionality"""
    
    def __init__(self, width, height):
        self._width = width
        self._height = height
        self._screen = pyte.Screen(width, height)
        self._stream = pyte.Stream(self._screen)
        
    @contextmanager
    def connect(self):
        """Connect to the PTT server with SSH."""
        with paramiko.SSHClient() as client:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # Ask PTT to use UTF-8 encoding so decoding is easier
            # with the risk of incorrect rendering in pyte
            client.connect('ptt.cc', username = 'bbsu', password = '')
            with client.invoke_shell(width=self._width, height=self._height) as channel:
                self._channel = channel
                yield self
       
    @contextmanager
    def login(self, username, passwd):
        """Login with the specified username and password."""
        # Read everything that's in the buffer first
        self.send(REFRESH)
        
        self.send(username + ENTER)
        self.send(passwd + ENTER)
        # XXX: Be optimisitic about being able to login
        assert not any('錯誤' in line for line in self.display())
        self.send(ENTER)
        
        try:
            yield self
        finally:
            if not self._channel.closed:
                self.send(LEFT)
                self.send('G' + ENTER)
                self.send('Y' + ENTER)
                self.send(ENTER)
    
    @contextmanager
    def chat_menu(self):
        """Enter the chat menu (CTRL+u)."""
        self.send(asci.ctrl('u'))
        try:
            yield self
        finally:
            if not self._channel.closed:
                self.send(LEFT)
                
    def online_users(self):
        """Returns a generator for username of users currently online."""
        with self.chat_menu():
            while True:
                for line in self.display()[3:-1]:
                    yield USERNAME_REGEX.search(line)[1]
                self.send(PAGEDOWN)
    
    def display(self):
        """Return the current screen."""
        return self._screen.display[:]
    
    def send(self, message):
        """Send string to the PTT server and return the updated screen"""
        self._channel.send(message)
        reply = self._read_all()
        self._stream.feed(reply)
        return self.display()

    def _read_all(self):
        """Try to read everything from the server, and return what's read."""
        data = b''
        time.sleep(CONTINIOUS_POLL)
        while self._channel.recv_ready():
            data += self._channel.recv(BUFF_SIZE)
            time.sleep(CONTINIOUS_POLL)
        return data.decode('utf-8', errors='ignore')


def main():
    ptt = PTT(WIDTH, HEIGHT)
    with ptt.connect(), \
            ptt.login(ACCOUNT['id'], ACCOUNT['passwd']):
        # We only want unique username
        users = set()
        for username in ptt.online_users():
            users.add(username)
            if len(users) >= 100:
                break
    print('\n'.join(sorted(users)))

def disable_paramiko_warning():
    """Don't show CryptographyDeprecationWarning"""
    import warnings
    from cryptography.utils import CryptographyDeprecationWarning
    warnings.filterwarnings(
        "ignore",
        category=CryptographyDeprecationWarning,
        module=r'paramiko\..*')


if __name__ == '__main__':
    disable_paramiko_warning()
    main()
